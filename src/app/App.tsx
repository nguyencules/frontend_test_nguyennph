import React, {  } from 'react';
import { Switch, Route,  } from 'react-router';
// Layout
import AppLayout from 'src/app/layout/AppLayout';
import {AccountLayout}  from 'src/app/layout/AccountLayout';

export const App: React.FC = () => {
  return (
    <Switch>
      <Route path="/account" component={AccountLayout} />
      <Route path="/" component={AppLayout} />            
    </Switch>
  );
};