import { Deserializable } from './deserializable.model';

export class SingleUserOutput implements Deserializable<SingleUserOutput> {
    id!: string;
    avatar!: string;
    firstName!: string;
    progress!: number;
    amount!: number;
    deadline!: string;


    deserialize(input: any): SingleUserOutput {
        this.id = input.id ? input.id: "";
        this.avatar = input.avatar ? input.avatar : "";
        this.firstName = input.firstName ? input.firstName : "";
        this.progress = input.progress ? input.progress : 0;
        this.amount = input.amount ? input.amount : 0;
        this.deadline = input.deadline ? input.deadline : "";
        return this;
    }
}