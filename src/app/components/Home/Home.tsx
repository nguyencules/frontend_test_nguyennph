import React, { } from 'react';

// State
import { UsersState } from 'src/app/store/reducers/state';
import { SingleUserOutput } from 'src/app/models/users.modal';

export interface Props {
  listUsers: UsersState;
}
export interface State {
}

const HomeComponent: React.FC<Props> = (props) => {
    const {listUsers} = props;
    console.log(listUsers)
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>
              Home
            </th>
            <th>
              First name
            </th>
            <th>
              Progress
            </th>
            <th>
              Amount
            </th>
            <th>
              Deadline
            </th>
          </tr>
        </thead>
        <tbody>
          {listUsers.listUsers.data.map((user: SingleUserOutput, index: number) => 
          <tr key={user.id}>
            <td className="py-1">
              <img src={user.avatar} alt="lockScreenBg" />
            </td>
            <td>
              {user.firstName}
            </td>
            <td>
              <div className="progress">
                <div className="progress-bar bg-success" role="progressbar" style={{width: `${user.progress}`}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
              </div>
            </td>
            <td>
              {`$ ${user.amount}`}
            </td>
            <td>
            {user.deadline}
            </td>
          </tr>
          )}          
        </tbody>
      </table>
)}
export default HomeComponent;