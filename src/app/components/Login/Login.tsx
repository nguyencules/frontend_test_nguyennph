import React, { useState } from 'react';
import { Link } from 'react-router-dom';

export interface Props {
    handleLogin: (input: {username: string, password: string}) => void;
}
export interface State {
    username: string;
    password: string;
}

export const LoginComponent: React.FC<Props> = (props) => {
    const [state, setState] = useState({
        username: "",
        password: "",
    });

    const _handleChangeState = (key: string, value: string) => {
        setState((prevState: State) => ({
            ...prevState,
            [key] : value
        }))
    };
    const _handleSbmit = (e: any) => {
        e.preventDefault(); 
        let value = {
            username: state.username,
            password: state.password
        };
        props.handleLogin(value);
    };
  return (
    <form className="pt-3" onSubmit={(e) => _handleSbmit(e)} >
        <div className="form-group">
            <input className="form-control form-control-lg" id="exampleInputEmail1" value={state.username} onChange={(e: any) => _handleChangeState("username", e.target.value)} placeholder="Email" />
        </div>
        <div className="form-group">
            <input type="password" className="form-control form-control-lg" id="exampleInputPassword1"  value={state.password} onChange={(e: any) => _handleChangeState("password", e.target.value)} placeholder="Password" />
        </div>
        <div className="mt-3">
            <button type="submit" onClick={(e) => _handleSbmit(e)} className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">SIGN IN</button>
        </div>
        <div className="my-2 d-flex justify-content-between align-items-center">
            <div className="form-check">
                <label className="form-check-label text-muted">
                    <input type="checkbox" className="form-check-input" />
                    Keep me signed in
                </label>
            </div>
            <Link to="#" className="auth-link text-black">Forgot password?</Link>
        </div>
        <div className="mb-2">
            <button type="button" className="btn btn-block btn-facebook auth-form-btn">
                <i className="ti-facebook mr-2" />Connect using facebook
            </button>
        </div>
        <div className="text-center mt-4 font-weight-light">
            Don't have an account? <Link to="/account/register" className="text-primary">Create</Link>
        </div>
    </form>
)}