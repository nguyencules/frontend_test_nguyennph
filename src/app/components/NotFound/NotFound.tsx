import React, { } from 'react';

export interface Props {
}

export const NotFoundComponent: React.FC<Props> = (props) => {
  return (
    <div style={{margin: "auto"}}> 
      <h1>404</h1>
    </div>
)}