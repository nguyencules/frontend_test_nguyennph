import { Dispatch } from 'redux';
import { createAction } from 'redux-actions';
export namespace RootActions {
    export const resetStore = createAction("RESET_STORE");
    export const handelResetStore = () => (dispatch: Dispatch) => {
        dispatch(resetStore());
    };
}