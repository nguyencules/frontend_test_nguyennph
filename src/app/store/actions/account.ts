import { Dispatch } from 'redux';
import { message } from 'antd';
import {loginApi} from 'src/utils/api';
import {setToken, delToken} from 'src/utils/token';

export const handleLogin = (input: { username: string; password: string; }, cb: (isSuccess: boolean) => void) => (dispatch: Dispatch) => {
    loginApi(input).then((res: any) => {
        if(res && res.status === 200) {
            let data = res.data;
            let token: string = "";

                token = data.token;
                setToken("token", token);
            cb(true);
            
        } else {
            message.error("Đã có vấn đề về xác thực, xin đăng nhập lại!");
            cb(false);
        }        
    });
};

export const handleLogout = (cb: (isSuccess: boolean) => void) => (dispatch: Dispatch) => {
    delToken("token");
    cb(true);
};
