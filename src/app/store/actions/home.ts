import { createAction } from 'redux-actions';
import { callAuthorizationApi } from 'src/utils/api';
import { Dispatch } from 'redux';
import { message } from 'antd';
import { listUsers } from '../reducers/state';
import { SingleUserOutput } from 'src/app/models/users.modal';

export const loadUsersList = createAction<{ listUsers: listUsers }>("LOAD_USERS_LIST");

export const loadUsers = () => (dispatch: Dispatch) => {
    callAuthorizationApi("users").then((res: any) => {
        if (!res.status) return;
        if(res.status === 200) {
                    let data = res.data;
            let userList: SingleUserOutput[] = [];
                  
            data.map((item: any, index: number) => {
                userList.push(new SingleUserOutput().deserialize(item));
            });
                dispatch(loadUsersList({
                    listUsers: Object.assign({}, {
                        data: userList
                    })
                }));
        } else {
            message.warning("Đã có lỗi về đường truyền vui lòng thử lại sau!")
            console.log("error");
        }
    });
};