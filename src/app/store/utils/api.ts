import axios from 'axios';
import {url} from './url';
export default function callApi( endpoint: string, method?: any, body?: any ) {
    return axios({
        method: method ? method : "GET",
        url: `${url}/${endpoint}`,
        data: body ? body : "",
    }).catch( (err: any) => {
        console.log(err);
    });
}