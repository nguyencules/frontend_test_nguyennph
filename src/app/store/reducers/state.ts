

import { SingleUserOutput } from 'src/app/models/users.modal';

export interface RootState {
    usersState: UsersState;
}
export type listUsers = {
    data: SingleUserOutput[];
};

export type UsersState = {
    listUsers: listUsers
};

/* Service */

// Label
