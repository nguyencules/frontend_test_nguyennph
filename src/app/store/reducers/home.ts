import { handleActions, combineActions } from 'redux-actions';
import { UsersState } from './state';
import { loadUsersList } from '../actions';
let initalState: UsersState = {
    listUsers: {
        data: []
    }
};
const homeReducer = handleActions(
    {
        [combineActions(
            loadUsersList,
        ).toString()]: (state: UsersState, action: any) => {
            return { ...state, ...action.payload };
        }
    },
    initalState
);
export default homeReducer;