import { combineReducers, Reducer } from 'redux';
import { RootState } from './state';
import homeReducer from './home';

const appReducer: Reducer<RootState> = combineReducers<RootState>({
    usersState: homeReducer,
});


const myReducer = (initalState: RootState | undefined, action: any) => {
    switch (action.type) {
        case "RESET_STORE":
            initalState = undefined;
            break;
    }
    return appReducer(initalState, action);
};
export default myReducer;