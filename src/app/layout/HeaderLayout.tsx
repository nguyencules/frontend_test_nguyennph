import React, { useState } from 'react';
import { Link,} from 'react-router-dom';

//img
import logo from 'src/assets/images/logo.svg';
import logoMini from 'src/assets/images/logo-mini.svg';
import face4 from 'src/assets/images/faces/face4.jpg';

export interface Props {
  handleLogout: () => void;
}
export interface State {
  isShowProfile: boolean;
}

export const HeaderLayout: React.FC<Props> = (props) => {

  const [ state, setState ] = useState({
    isShowProfile: false,

  });

  const toggleState = () => {    
    setState((prevState: State) => ({
        ...prevState,
        isShowProfile: !state.isShowProfile,
    }));    
  };

  const _handleLogout = () => {
    props.handleLogout();
  }
  return (
      <nav className="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
          <Link className="navbar-brand brand-logo mr-5" to="../../index.html"><img src={logo} className="mr-2" alt="logo" /></Link>
          <Link className="navbar-brand brand-logo-mini" to="../../index.html"><img src={logoMini} alt="logo" /></Link>
        </div>
        <div className="navbar-menu-wrapper d-flex align-items-center justify-content-end">
          <button className="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span className="ti-view-list" />
          </button>
          <ul className="navbar-nav mr-lg-2">
            <li className="nav-item nav-search d-none d-lg-block">
              <div className="input-group">
                <div className="input-group-prepend hover-cursor" id="navbar-search-icon">
                  <span className="input-group-text" id="search">
                    <i className="ti-search" />
                  </span>
                </div>
                <input type="text" className="form-control" id="navbar-search-input" placeholder="Search now" aria-label="search" aria-describedby="search" />
              </div>
            </li>
          </ul>
          <ul className="navbar-nav navbar-nav-right">
            <li className="nav-item dropdown mr-1">
              <Link className="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" to="#" data-toggle="dropdown">
                <i className="ti-email mx-0" />
              </Link>
              <div className="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="messageDropdown">
                <p className="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                <Link className="dropdown-item" to="">
                  <div className="item-thumbnail">
                    <img src={face4} alt="Avatar" className="profile-pic" />
                  </div>
                  <div className="item-content flex-grow">
                    <h6 className="ellipsis font-weight-normal">David Grey
                    </h6>
                    <p className="font-weight-light small-text text-muted mb-0">
                      The meeting is cancelled
                    </p>
                  </div>
                </Link>
                <Link className="dropdown-item" to="">
                  <div className="item-thumbnail">
                    <img src={face4} alt="Avatar" className="profile-pic" />
                  </div>
                  <div className="item-content flex-grow">
                    <h6 className="ellipsis font-weight-normal">Tim Cook
                    </h6>
                    <p className="font-weight-light small-text text-muted mb-0">
                      New product launch
                    </p>
                  </div>
                </Link>
                <Link className="dropdown-item" to="">
                  <div className="item-thumbnail">
                    <img src={face4} alt="Avatar" className="profile-pic" />
                  </div>
                  <div className="item-content flex-grow">
                    <h6 className="ellipsis font-weight-normal"> Johnson
                    </h6>
                    <p className="font-weight-light small-text text-muted mb-0">
                      Upcoming board meeting
                    </p>
                  </div>
                </Link>
              </div>
            </li>
            <li className="nav-item dropdown">
              <Link className="nav-link count-indicator dropdown-toggle" id="notificationDropdown" to="#" data-toggle="dropdown">
                <i className="ti-bell mx-0" />
                <span className="count" />
              </Link>
              <div className="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="notificationDropdown">
                <p className="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                <Link className="dropdown-item" to="">
                  <div className="item-thumbnail">
                    <div className="item-icon bg-success">
                      <i className="ti-info-alt mx-0" />
                    </div>
                  </div>
                  <div className="item-content">
                    <h6 className="font-weight-normal">Application Error</h6>
                    <p className="font-weight-light small-text mb-0 text-muted">
                      Just now
                    </p>
                  </div>
                </Link>
                <Link className="dropdown-item" to="">
                  <div className="item-thumbnail">
                    <div className="item-icon bg-warning">
                      <i className="ti-settings mx-0" />
                    </div>
                  </div>
                  <div className="item-content">
                    <h6 className="font-weight-normal">Settings</h6>
                    <p className="font-weight-light small-text mb-0 text-muted">
                      Private message
                    </p>
                  </div>
                </Link>
                <Link className="dropdown-item" to="">
                  <div className="item-thumbnail">
                    <div className="item-icon bg-info">
                      <i className="ti-user mx-0" />
                    </div>
                  </div>
                  <div className="item-content">
                    <h6 className="font-weight-normal">New user registration</h6>
                    <p className="font-weight-light small-text mb-0 text-muted">
                      2 days ago
                    </p>
                  </div>
                </Link>
              </div>
            </li>
            <li className="nav-item nav-profile dropdown">
              <div className="nav-link dropdown-toggle" onClick={toggleState} data-toggle="dropdown" id="profileDropdown">
                <img src={face4} alt="profile" />
              </div>
              <div className={`dropdown-menu dropdown-menu-right navbar-dropdown ${state.isShowProfile ? "show" : ""}`} aria-labelledby="profileDropdown">
                <Link className="dropdown-item" to="">
                  <i className="ti-settings text-primary" />
                  Settings
                </Link>
                <button className="dropdown-item" onClick={_handleLogout}>
                  <i className="ti-power-off text-primary" />
                  Logout
                </button>
              </div>
            </li>
          </ul>
          <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span className="ti-view-list" />
          </button>
        </div>
      </nav>
)};