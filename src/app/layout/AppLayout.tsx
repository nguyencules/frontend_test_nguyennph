import React, { useEffect } from 'react';
import { Switch, Route,  } from 'react-router';
import { History } from 'history';

// Redux
import { bindActionCreators, Dispatch } from 'redux';
import * as AccountActions from 'src/app/store/actions/account';
import { connect } from 'react-redux';

// Layout
import {DrawerLayout } from 'src/app/layout/DrawerLayout';
import {FooterLayout } from 'src/app/layout/FooterLayout';
import {HeaderLayout } from 'src/app/layout/HeaderLayout';

// Content
import HomeContainer  from 'src/app/containers/Home/HomeContainer';
import {NotFoundComponent } from 'src/app/components/NotFound/NotFound';

// Util
import { getToken } from 'src/utils/token';

export interface Props {
    history: History;
    actions: typeof AccountActions;
}

function mapDispatchToProps(dispatch: Dispatch): Pick<Props,"actions"> {
    return { actions: bindActionCreators(Object.assign(AccountActions), dispatch) }
}

const AppLayout: React.FC<Props> = (props) => {
    useEffect(() => {
        let token = getToken("token");
        if (!token) {
            props.history.replace("/account/login");
        }
    },[]);
      
    const _handleLogout = () => {
      props.actions.handleLogout(_cbLogin);    
    };

    const _cbLogin = (isSuccess: boolean) => {
        if (isSuccess) {
            props.history.push("/account/login");
        }
    };
    return (
        <div className="container-scroller">
            <HeaderLayout handleLogout={_handleLogout}/>
            <div className="container-fluid page-body-wrapper row">
                <DrawerLayout/>
                <Switch>
                    <Route  exact={true} path="/" component={HomeContainer} />
                    <Route  exact={true} path="/home" component={HomeContainer} />                    
                    <Route  exact={true} path="*" component={NotFoundComponent} />
                </Switch>
            </div>
            <FooterLayout/>
        </div>
    )};

export default connect (null, mapDispatchToProps)(AppLayout);
            