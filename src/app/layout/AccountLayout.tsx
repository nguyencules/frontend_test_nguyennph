import React, { } from 'react';
import { History } from 'history';
import { Switch, Route } from 'react-router-dom';

/* Containers */
import  LoginContainer from 'src/app/containers/Account/LoginContainer';
import { RegisterContainer } from 'src/app/containers/Account/RegisterContainer';

/* Type */

export interface Props {
    history: History;
}

export const AccountLayout: React.FC<Props> = (props) => {
    return (
        <div className="account">
            <Switch>                
                <Route path="/account/login" component={LoginContainer} />
                <Route path="/account/register" component={RegisterContainer} />
            </Switch>
        </div>
    );
};
