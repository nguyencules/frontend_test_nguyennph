import React, { } from 'react';
import { History } from 'history';

// Redux
import { bindActionCreators, Dispatch } from 'redux';
import * as AccountActions from 'src/app/store/actions/account';
import { connect } from 'react-redux';

//COMPONENT
import {LoginComponent} from 'src/app/components/Login/Login';

export interface Props {
  history: History;
  actions: typeof AccountActions;
}
export interface State {
  actioning: boolean;
}


function mapDispatchToProps(dispatch: Dispatch): Pick<Props,"actions"> {
return { actions: bindActionCreators(Object.assign(AccountActions), dispatch) }
}
class LoginContainer extends React.Component<Props, State> {
  state = {
    actioning: false,
  }
  _handleChangeState = (key:string, value: any ) => {
    this.setState((prevState: State) => ({
      ...prevState,
      [key] : value,
    }));
  };
  
  _handleLogin = (input: {username: string, password: string}) => {
    this.props.actions.handleLogin(input, this._cbLogin);    
  };

  _cbLogin = (isSuccess: boolean) => {
    this._handleChangeState("actioning", false);
    console.log(isSuccess);
    console.log(this.props.history.action)
    if (isSuccess) {
        if (this.props.history.action === "PUSH") {
            this.props.history.go(-1);
        } else {
            this.props.history.push("/");
        }
    }
  };
  render() {
    return (
      <div className="container-scroller">
          <div className="container-fluid page-body-wrapper full-page-wrapper row">
              <div className="content-wrapper d-flex align-items-center auth px-0">
                  <div className="row w-100 mx-0">
                      <div className="col-lg-4 mx-auto">
                        <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                          <div className="brand-logo">
                              <img src="../../images/logo.svg" alt="logo" />
                          </div>
                          <h4>Hello! let's get started</h4>
                          <h6 className="font-weight-light">Sign in to continue.</h6>
                          <LoginComponent handleLogin={this._handleLogin}/>
                        </div>
                    </div>
                  </div>
              </div>
          </div>
      </div>
    )
  }
}
export default connect (null, mapDispatchToProps)(LoginContainer);