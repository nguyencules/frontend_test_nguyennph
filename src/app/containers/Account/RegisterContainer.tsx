import React, { } from 'react';

//COMPONENT
import {RegisterComponent} from 'src/app/components/Register/Register';

export interface Props {
}
export interface State {
}

export const RegisterContainer: React.FC<Props> = (props) => {
  return (
    <div className="container-scroller">
        <div className="container-fluid page-body-wrapper full-page-wrapper row">
            <div className="content-wrapper d-flex align-items-center auth px-0">
                <div className="row w-100 mx-0">
                    <div className="col-lg-4 mx-auto">
                        <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                            <div className="brand-logo">
                              <img src="../../images/logo.svg" alt="logo" />
                            </div>
                            <h4>New here?</h4>
                            <h6 className="font-weight-light">Signing up is easy. It only takes a few steps</h6>
                            <RegisterComponent/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)}