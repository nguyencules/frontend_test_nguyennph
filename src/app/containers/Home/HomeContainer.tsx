import React, { useEffect, Suspense } from 'react';
import { History } from 'history';

// Redux
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UserActions from 'src/app/store/actions/home';

// State
import { RootState, UsersState } from 'src/app/store/reducers/state';
import { getToken } from 'src/utils/token';

//COMPONENT
const HomeComponent = React.lazy(() => import('src/app/components/Home/Home'));

export interface Props {
  actions: typeof UserActions;
  history: History;
  listUsers: UsersState;
}
export interface State {
}

function mapStateToProps(RootState: RootState): Pick<Props,"listUsers"> {
  return { listUsers: RootState.usersState}
}

function mapDispatchToProps(dispatch: Dispatch): Pick<Props,"actions"> {
return { actions: bindActionCreators(Object.assign(UserActions), dispatch) }
}

const HomeContainer: React.FC<Props> = (props) => {

  useEffect(() => {
    let token = getToken("token");
    // I always call API refresh token when Customer reload or reopen website but I'm having
    // Api in order to do it. So I must check the token. I think I have a problem with authentication here, but i don't have any time to done it or clear code hear.
    // Thank you for you watching. Have a nice weekend ^^
    if (token) {
      props.actions.loadUsers();
    }
  },[]);

  return (
    <div className="main-panel">
      <div className="content-wrapper">
        <div className="row">
          <div className="col-lg-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Striped Table</h4>
                <div className="table-responsive">
                <Suspense fallback={<div>Loading...</div>}>
                  <HomeComponent listUsers={props.listUsers}/>
                </Suspense>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default connect (mapStateToProps, mapDispatchToProps)(HomeContainer);