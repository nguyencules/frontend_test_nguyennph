import axios from 'axios';
import {env} from 'src/env/env';

export const loginApi = ( body: any ) => {
    const headers = { 'Content-Type': 'application/json' };
    return axios({
        ...headers,
        method: "POST",
        url: `${env.constants.api_domain}/login`,
        data: body,
    }).catch( (err: any) => {
        console.log(err);
    });
}

export const callAuthorizationApi = ( endpoint: string, method?: any, body?: any, headers = { 'Content-Type': 'application/json' }) => {
    const token = localStorage.getItem('token');
    return axios({
        method: method ? method : "GET",
        url: `${env.constants.api_domain}/${endpoint}`,
        data: body ? body : "",
        headers: { 
            ...headers, 
            Accept: 'application/json',
            Authorization: `Bearer ${token}`
        },            
    }).catch( (err: any) => {
        console.log(err);
    });
  };