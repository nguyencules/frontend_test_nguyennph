// token
    export const setToken: any = (name: string, value: any) => {
        localStorage.setItem(name, value);
    };

    export const getToken: any = (name: string) => {
        return localStorage.getItem(name);
    };

    export const delToken: any = (name: string) => {
        localStorage.removeItem(name);
    };


