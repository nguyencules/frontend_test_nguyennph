import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createBrowserHistory } from "history";
import { Route } from "react-router-dom";
import { Router } from 'react-router';
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from 'react-redux';
import { App } from 'src/app/App';
import thunk from 'redux-thunk';
import myReducer from './app/store/reducers/index';
import "antd/dist/antd.css";
import "./assets/scss/style.scss";
import "./assets/css/style.css";
import 'src/../node_modules/bootstrap/dist/css/bootstrap.min.css';
import 'src/../node_modules/bootstrap/dist/css/bootstrap.css';
import moment from 'moment-timezone';
moment.tz.setDefault("Asia/Ho_Chi_Minh");
const history = createBrowserHistory();

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(myReducer, compose(applyMiddleware(thunk), composeEnhancers()));
ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
